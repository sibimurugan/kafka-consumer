package com.radial.configuration;


import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.kafka.ConcurrentKafkaListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.KafkaOperations;
import org.springframework.kafka.listener.ContainerProperties.AckMode;
import org.springframework.kafka.listener.DeadLetterPublishingRecoverer;
import org.springframework.kafka.listener.SeekToCurrentErrorHandler;
import org.springframework.util.backoff.FixedBackOff;

@EnableKafka
@Configuration
public class KafkaListenerConfiguration {

    @Value("${spring.kafka.bootstrap-servers}")
    private String bootstrapServers;
    
    
    @Value("${kafka.listener.groupId}")
    private String groupId;
    
    
    @Value("${kafka.listener.concurrency}")
    private int setConcurrency;

    @Bean
    public Map<String, Object> consumerConfigs() {
        Map<String, Object> props = new HashMap<>();
	    props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);
        props.put(ConsumerConfig.ISOLATION_LEVEL_CONFIG, "read_committed");
        props.put(ConsumerConfig.HEARTBEAT_INTERVAL_MS_CONFIG, 10000);
        props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, 30000);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
       // props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, maxRecordPoll);
        return props;
    }

    @Bean
    public ConsumerFactory<Object, Object> consumerFactory() {
        return new DefaultKafkaConsumerFactory<>(consumerConfigs());
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<Object, Object> kafkaListenerContainerFactory (
	    ConcurrentKafkaListenerContainerFactoryConfigurer configurer,  ConsumerFactory<Object, Object> kafkaConsumerFactory,
	    KafkaOperations<? extends Object, ? extends Object> template ) {
    	ConcurrentKafkaListenerContainerFactory<Object, Object> factory = new ConcurrentKafkaListenerContainerFactory<>();
    	configurer.configure(factory, consumerFactory());
    	DeadLetterPublishingRecoverer recoverer = new DeadLetterPublishingRecoverer(template);
    	factory.getContainerProperties().setAckMode(AckMode.RECORD);
    	factory.setErrorHandler(new SeekToCurrentErrorHandler(recoverer, new FixedBackOff(2L, 1)));
    	factory.setConcurrency(setConcurrency);
    	return factory;
    }
    
    
    
    
    /**
     * Batch Listener 
     
    @Bean
    public ConcurrentKafkaListenerContainerFactory<Object, Object> kafkaListenerContainerFactoryORT (
	    ConcurrentKafkaListenerContainerFactoryConfigurer configurer,  ConsumerFactory<Object, Object> kafkaConsumerFactory,
	    KafkaOperations<? extends Object, ? extends Object> template ) {
    		ConcurrentKafkaListenerContainerFactory<Object, Object> factory = new ConcurrentKafkaListenerContainerFactory<>();
    		configurer.configure(factory, consumerFactory());
    		DeadLetterPublishingRecoverer recoverer = new DeadLetterPublishingRecoverer(template);	
    		factory.setBatchListener(true);	
    		factory.getContainerProperties().setAckMode(AckMode.MANUAL);
    		RecoveringBatchErrorHandler errorHandler = new RecoveringBatchErrorHandler(recoverer, new FixedBackOff(2L, 1));
    		factory.setBatchErrorHandler(errorHandler);
    		factory.setConcurrency(setConcurrency);
    		return factory;
    }*/
    
}
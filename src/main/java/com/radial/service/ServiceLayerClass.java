package com.radial.service;

import org.springframework.stereotype.Service;

@Service
public class ServiceLayerClass {
	
	public void processBusiness(Object obj) {
		System.out.println("Business done--> "+obj);
	}

}

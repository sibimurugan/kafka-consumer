package com.radial.consumer;

/**
 * @author bselvam
 * This Class is the Entry point to the Transformation Layer. 
 * The below KafkaListener listen to the Topic where orders are routed from Router Application. 
 * Once this method is ignited, We'll get the Java object and does the TXML transformation and drops to WMOS Warehouse Queue. 
 */
import java.io.IOException;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.radial.service.ServiceLayerClass;

@Service
public class Consumer {
	
	@Autowired
	ObjectMapper objectmapper;
	
	@Autowired
	public ServiceLayerClass serviceCall;
	
	 @Value("${kafka.listener.topic}")
     private String listenerTopic;
	 
	 
	/**
	 * This method consumes the message from Topic. 
	 * @throws IOException
	 * */
	 @KafkaListener(topics = "${kafka.listener.topic}" ,containerFactory = "kafkaListenerContainerFactory", id ="${kafkaconsumerprefix}")
	    public void consume(ConsumerRecord<String, String> records) throws IOException {
		 
		 /**
		  * To extract the headers
		  * we can use below code. 
		  */
		 
			for (org.apache.kafka.common.header.Header header : records.headers()) {    
				/*
				 * We can have our code to retrive the header message.
				 */
			}
			
			/**
			 * To retrive the Message Body
			 */		 
			
			Object obj = records.value();
			
			/**
			 * performing business Task
			 */
			
			serviceCall.processBusiness(obj);
		 
	 }
	 
	 
	 
}	 
	 
